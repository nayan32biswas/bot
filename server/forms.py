from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField




class MessageForm(FlaskForm):
    message = StringField('Message')
    submit = SubmitField('Send')

class TrainForm(FlaskForm):
    story = TextAreaField("Story")
    train = SubmitField('Train')