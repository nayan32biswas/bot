from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer


chatbot = ChatBot(
    'ChatBot',
    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    database_uri='sqlite:///chatbot.sqlite3'
)


def trainTheChatBot(story):
    trainer = ListTrainer(chatbot)
    trainer.train(story)


def process_message(sender_id, message):
    response = chatbot.get_response(message)
    return response
