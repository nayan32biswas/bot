import datetime
import os
import logging

from flask import Flask, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy
from pymessenger import Bot

from forms import MessageForm, TrainForm
from text_processor import process_message, trainTheChatBot

app = Flask(__name__)
app.config["SECRET_KEY"] = "demodemodemo"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///server.sqlite3"
db = SQLAlchemy(app)
session = db.session

VERIFY_TOKEN = os.environ.get("VERIFY_TOKEN")
FACEBOOK_ACCESS_TOKEN = os.environ.get("FACEBOOK_ACCESS_TOKEN")

py_bot = Bot(FACEBOOK_ACCESS_TOKEN)


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username_id = db.Column(db.String(50), unique=True, nullable=False)


class Message(db.Model):
    __tablename__ = 'message'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='messages', lazy=True)

    message = db.Column(db.String, nullable=False)
    reply = db.Column(db.String, nullable=True)


def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance, True


def save_message_data(username_id, message, reply):
    try:
        user, crated = get_or_create(session, User, username_id=username_id)
        session.add(Message(
            user_id=user.id,
            message=message,
            reply=reply,
        ))
        session.commit()
    except Exception as error:
        print(error)


@app.route("/", methods=["GET"])
def home(*args, **kwargs):
    return render_template("home.html")


messages = []


@app.route("/message/", methods=['GET', 'POST'])
def message_bot():
    form = MessageForm()
    if form.validate_on_submit():
        username = "123123"
        msg = form.message.data
        reply = process_message(username, msg)
        messages.append({
            "message": form.message.data,
            "reply": reply
        })
        form.message.data = ""
        return render_template('message.html', title='Message', messages=messages, form=form)
    return render_template('message.html', title='Message', messages=messages, form=form)




@app.route("/train/", methods=["GET", "POST"])
def train(*args, **kwargs):
    form = TrainForm()
    if form.validate_on_submit():
        data = form.story.data
        story = data.splitlines()
        trainTheChatBot(story)
        return render_template("train.html", form=form, head_title="Train Successfully")
    return render_template("train.html", form=form, head_title="Write a conversation")


@app.route("/facebook/callback/", methods=["GET", "POST"])
def facebook_bot(*args, **kwargs):
    if request.method == "GET":
        if request.args.get("hub.verify_token") == VERIFY_TOKEN:
            return request.args.get("hub.challenge")
        else:
            return "Token is not valid."
    elif request.method == "POST":
        payload = request.json
        event = payload["entry"][0]["messaging"]
        for msg in event:
            text = msg["message"]["text"]
            sender_id = msg["sender"]["id"]
            reply = process_message(sender_id, text)
            save_message_data(sender_id, text, reply)
            py_bot.send_text_message(sender_id, reply)
        return "MESSAGE RECEIVED."
    else:
        print(request.form)
        return "METHOD NOT VALID"


if __name__ == "__main__":
    app.run(debug=True)
